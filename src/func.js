const getSum = (str1, str2) => {
  if (isNaN(str1) || isNaN(str2) || typeof str1 === "object" || typeof str2 === "object" || typeof str1 === "number" || typeof str2 === "number") { return false; }
  str1 = str1.trim() === "" ? 0 : parseFloat(str1);
  str2 = str2.trim() === "" ? 0 : parseFloat(str2);
  return `${str1 + str2}`;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = listOfPosts.filter(post => post.author === authorName).length;
  let comments = listOfPosts.reduce((quantityOfComments, post) => {
    return quantityOfComments + (post.comments === undefined ? 0 : post.comments.filter(comment => comment.author === authorName).length);
  }, 0);
  return `Post:${posts},comments:${comments}`;
};

const tickets=(people)=> {
  const oneLen = people.filter(item => item === 25)
  const twoLen = people.filter(item => item === 50)
  const capLen = 100 / 25
  return oneLen.length + twoLen.length >= capLen ? 'YES' : 'NO'
}

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
